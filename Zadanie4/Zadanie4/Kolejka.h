#pragma once

#include "Wezel.h"
using namespace std;
template <typename T> class Kolejka {
	Wezel<T> * glowa;
	Wezel<T> * ogon;
	unsigned int rozmiar;
public:
	Kolejka() {
		glowa = nullptr;
		ogon = nullptr;
		rozmiar = 0;
	}
	void dodaj(T _wartosc);
	void usun();
	void przod();
	unsigned int getRozmiar() const;
	bool czyPusta();
};

template <typename T>
void Kolejka<T>::dodaj(T _wartosc) {
	Wezel<T> * nowy = new Wezel<T>;
	nowy->setWartosc(_wartosc);
	if (czyPusta()) {
		glowa = nowy;
		ogon = nowy;
	}
	else {
		ogon->setNastepny(nowy);
		ogon = nowy;
	}
	rozmiar++;
}

template <typename T>
void Kolejka<T>::usun() {
	Wezel<T> * tmp = glowa;
	if (!czyPusta()) {
		if (tmp == ogon) {
			delete tmp;
			ogon = nullptr;
			glowa = nullptr;
		}
		else {
			glowa = tmp->getNastepny();
			delete tmp;
		}
		rozmiar--;
	}
}

template <typename T>
void Kolejka<T>::przod() {
	if (!czyPusta()) {
		cout << glowa->getWartosc() << endl;
	}
	else cout << "Lista jest pusta!" << endl;
}

template <typename T>
bool Kolejka<T>::czyPusta() {
	if (glowa)
		return false;
	return true;
}

template <typename T>
unsigned int Kolejka<T>::getRozmiar() const{
	return rozmiar;
}
