// Zadanie4.cpp
// Amadeusz Wach

#include "stdafx.h"
#include<iostream>
#include "Kolejka.h"

int main()
{
	Kolejka<int> K;

	bool czyKoniec = false;
	int wybor;
	system("cls");
	cout << "1. Dodaj element na koniec kolejki" << endl;
	cout << "2. Usun element z przodu kolejki" << endl;
	cout << "3. Wyswietl element z przodu" << endl;
	cout << "5. Wyswietl rozmiar kolejki" << endl;
	
	cout << "6. Koniec programu" << endl;
	string word;
	while (!czyKoniec) {
		cout << "Twoj wybor: ";
		cin >> wybor;
		switch (wybor) {
		case 1: {
			int wartosc;
			cout << "Podaj wartosc: ";
			cin >> wartosc;
			K.dodaj(wartosc);
			break;
		}
		case 2: {
			K.usun();
			break;
		}

		case 3: {
			K.przod();
			break;
		}
		case 4: {
			cout << "Rozmiar" << K.getRozmiar() << endl;
			break;
		}
		case 5: {
			czyKoniec = true;
			break;
		}

		default: {
			cout << "Nie znaleziono opcji, sprobuj ponownie!" << endl;
		}
		}
	}

	getchar();
	return 0;
}

