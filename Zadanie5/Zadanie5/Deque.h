#pragma once
#include "Wezel.h"
using namespace std;
template <typename Object>
class Deque {
	Wezel<Object> * glowa;
	Wezel<Object> * ogon;

	int _rozmiar;
public:
	Deque();
	/**
	* Zwraca ilo�� obiekt�w przechowywanych w deque
	*/
	int rozmiar() const;
	/**
	* Zwraca true je�li deque jest pusty
	*/
	bool isEmpty() const;
	/**
	* Zwraca pierwszy obiekt w deque.
	* Wyrzuca DequeEmptyException je�li deque jest pusty
	*/
	Object& pierwszy();
	/**
	* Zwraca ostatni obiekt w deque.
	* Wyrzuca DequeEmptyException je�li deque jest pusty
	*/
	Object& ostatni();
	/**
	* Dodaje obiekt do pocz�atku deque�a.
	*/
	Object& wstawPierwszy(const Object& obj);
	/**
	* Usuwa pierwszy obiekt z deque.
	* Wyrzuca DequeEmptyException je�li deque jest pusty
	*/
		Object& usunPierwszy() throw(DequeEmptyException);
		/**
		* Dodaje obiekt na ko�cu deque�a.
		*/
		Object& wstawOstatni(const Object& obj);
	/**
	* Usuwa ostatni obiekt z deque.
	* Wyrzuca DequeEmptyException je�li deque jest pusty
	*/
	Object& usunOstatni() throw(DequeEmptyException);
};

template <typename Object>
Deque<Object>::Deque() {
	glowa = nullptr;
	ogon = nullptr;
	_rozmiar = 0;
}

template <typename Object>
int Deque<Object>::rozmiar() const{
	return _rozmiar;
}

template <typename Object>
bool Deque<Object>::isEmpty() const {
	if (glowa)
		return false;
	return true;
}

template <typename Object>
Object& Deque<Object>::pierwszy(){
	if (!isEmpty()) {
		return &glowa;
	}
	else throw(DequeEmptyException);
}

template <typename Object>
Object& Deque<Object>::ostatni() {
	if (!isEmpty()) {
		return &ogon;
	}
	else throw(DequeEmptyException);
}
template <typename Object>
Object& Deque<Object>::wstawPierwszy(const Object& obj) {

}