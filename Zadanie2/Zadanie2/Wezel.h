#pragma once
template <typename T>
class Wezel {
	T wartosc;
	Wezel<T> * nastepny;
public:
	Wezel();

	Wezel<T> * getNastepny() const;
	void setNastepny(Wezel<T> * _nastepny);
	T getWartosc () const;
	void setWartosc(T _wartosc);

};
template <typename T>
Wezel<T>::Wezel() {
	nastepny = nullptr;
}
template <typename T>
Wezel<T> * Wezel<T>::getNastepny() const{
	return nastepny;
}
template <typename T>
void Wezel<T>::setNastepny(Wezel<T> * _nastepny){
	nastepny = _nastepny;
}
template <typename T>
T Wezel<T>::getWartosc() const {
	return wartosc;
}
template <typename T>
void Wezel<T>::setWartosc(T _wartosc){
	wartosc = _wartosc;
}

