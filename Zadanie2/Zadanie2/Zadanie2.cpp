// Zadanie2.cpp
// Amadeusz Wach

#include "stdafx.h"
#include<iostream>
#include "Lista.h"

int main()
{
	Lista<int> L;
	
	bool czyKoniec = false;
	int wybor;
	system("cls");
	cout << "1. Dodaj element z przodu listy" << endl;
	cout << "2. Dodaj element z tylu listy" << endl;
	cout << "3. Usun element z przodu listy" << endl;
	cout << "4. Usun wszystkie elemnty z listy" << endl;
	cout << "5. Wyswietl liste" << endl;
	cout << "6. Koniec programu" << endl;
	string word;
	while (!czyKoniec) {
		cout << "Twoj wybor: ";
		cin >> wybor;
		switch (wybor) {
		case 1: {
			int wartosc;
			cout << "Podaj wartosc: ";
			cin >> wartosc;
			L.dodajPrzod(wartosc);
			break;
		}
		case 2: {
			int wartosc;
			cout << "Podaj wartosc: ";
			cin >> wartosc;
			L.dodajTyl(wartosc);
			break;
		}
		case 3: {
			L.usunPrzod();
			break;
		}

		case 4: {
			L.usunWszystko();
			break;
		}
		case 5: {
			L.wyswietl();
			break;
		}
		case 6: {
			czyKoniec = true;
			break;
		}

		default: {
			cout << "Nie znaleziono opcji, sprobuj ponownie!" << endl;
		}
		}
	}

	getchar();
	return 0;
}

