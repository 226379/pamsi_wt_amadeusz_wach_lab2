// Zadanie1.cpp Lista 2
// Amadeusz Wach

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

vector<string> palList;

void usunDup() {
	sort(palList.begin(), palList.end());
	palList.erase(unique(palList.begin(), palList.end()), palList.end());
}

bool jestPal(string testStr) {
	if (testStr.size() == 1 || testStr.size() == 0) return true;
	if (testStr.front() == testStr.back())
		return jestPal(testStr.substr(1, testStr.size() - 2));
	else return false;
}

void permutacja(string testStr, unsigned int krok) {
	if (krok == 0) {
		if (jestPal(testStr))
			palList.push_back(testStr);
	}
	else {
		for (unsigned int i = 0; i <= krok; i++) {
			swap(testStr[i], testStr[krok]);
			permutacja(testStr, krok - 1);
			swap(testStr[i], testStr[krok]);
		}
	}

}


int main()
{
	bool isExit = false;
	int choice;
	system("cls");
	cout << "1. Generowanie permutacji" << endl;
	cout << "2. Usuniecie powtorzen" << endl;
	cout << "3. Wyswietlenie listy palindromow" << endl;
	cout << "4. Koniec programu" << endl;
	string word;
	while (!isExit) {
		cout << "Twoj wybor: ";
		cin >> choice;
		switch (choice) {
		case 1: {

			cout << "Podaj slowo: ";
			cin >> word;
			permutacja(word, word.size()-1);
			break;
		}

		case 2: {
			usunDup();
			break;
		}

		case 3: {
			for (unsigned int i = 0; i < palList.size(); i++)
				cout << palList[i] << " ";
			break;
		}

		case 4: {
			isExit = true;
			break;
		}

		default: {
			cout << "Nie znaleziono opcji, sprobuj ponownie!" << endl;
		}
		}
	}

	return 0;
}

